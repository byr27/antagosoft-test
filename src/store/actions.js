import Vue from 'vue';
import VueResource from 'vue-resource';

Vue.use(VueResource);

export default {
  addComment({ commit }, comment) {
    commit('updateState', comment);
  },
};
