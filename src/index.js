import Vue from 'vue';
import VueResource from 'vue-resource';
import VeeValidate from 'vee-validate';

import App from './App';
import store from './store';

import 'normalize.css';

Vue.use(VueResource);
Vue.use(VeeValidate);

const app = new Vue({
  store,
  render: h => h(App),
});

app.$mount('#app');
